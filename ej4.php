<!DOCTYPE html>
<html class="no-js" lang="es">

<head>
	<title>Tabla de Fotos</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="miestilo.css">
	
</head>

<h1>Tabla con Imagenes piolas</h1>
<body>
	<table>
		
    <?php
	//leo la carpeta donde se encuentran las fotos almacenadas
	$directory="fotos/";
    $dirint = opendir($directory);
	//abro la fila donde estaran las imagenes separadas en columnas
	echo("<tr>");
	//leo el directorio de las fotos
    while ($archivo = readdir($dirint)){
		echo("<td>");
		//indico el formato de las fotos a imprimir en la tabla
        if (strpos($archivo,'jpg') || strpos($archivo,'png')){
            $image = $directory. $archivo;
            //imprimo la imagen en cada columna
            echo'<img src='.$image. ' width="75%" height="75%" >';
        }
        echo("</td>");
    }
    //cierro el directorio
    closedir($dirint);
	echo("</tr>");
	?>
	</table>
</body>

</html>
