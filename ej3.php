<!DOCTYPE html>
<html class="no-js" lang="es">

<head>
	<title>Tablita Editable</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="miestilo.css">
	
</head>

<h1>Tabla</h1>
<body>
	<!--se arma las casillas donde se recibira lo solicitaro por el usuario-->
	<form action="" method="get">
		<label for="tamano">Tamaño de la tabla:</label>
		<input type="text" name="tamano"><br><br>
		<label for="color">Color a elección:</label>
		<input type="text" name="color"><br><br>
		<input type="submit" value="Ejecutar">
	</form>
	<table>
	<?php
		//variables a ocupar en la tabla en base a lo entregado
		$tamano = $_GET["tamano"];
		$color = $_GET["color"];
		$variable=0; 
		 
		
		for($vectorX=1;$vectorX<=$tamano; $vectorX++){
			echo("<tr>");
			for($vectorY=1;$vectorY<=$tamano; $vectorY++){
				$variable=$variable+1;
				if($vectorY%2){
					echo("<td style='background-color: $color;'>");
					echo("$variable");
				}else{
					echo("<td>");
					echo("$variable");
				}
				echo("</td>");
			}
			echo("</tr>\n");
		}
		?>	
	</table>
</body>

</html>
