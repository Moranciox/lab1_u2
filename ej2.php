<!DOCTYPE html>
<html class="no-js" lang="es">

<head>
	<title>Tabla GrisxBlanco</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="miestilo.css">
	
</head>

<h1>Tabla Gris Blanco</h1>
<body>
	<table>
	<?php
		//variables a ocupar en la tabla
		$tamano=20; 
		$variable=0; 
		 
		//armo un ciclo en el cual comienzo tanto una filca como una columna para imprimir los numeros
		for($vectorX=1;$vectorX<=$tamano; $vectorX++){
			echo("<tr>");
			for($vectorY=1;$vectorY<=$tamano; $vectorY++){
				$variable=$variable+1;
				//en base a cada columna par se le asigna el color gris al fondo de la casilla
				if($vectorY%2){
					echo("<td style='background-color: Gray;'>");
					echo("$variable");
				}else{
					echo("<td>");
					echo("$variable");
				}
				echo("</td>");
			}
			echo("</tr>\n");
		}
		?>	
	</table>
</body>

</html>
